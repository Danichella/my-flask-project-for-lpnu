import json
from app import flask_app, db
from models import User, Event

def generate_header():
  client = flask_app.test_client()
  request_params = {
    'email': "superuser@gmail.com",
    'password': "12345678",
  }
  token = client.post('/login', json=request_params).json['token']
  return {
    'x-access-tokens': token[2:len(token) - 1]
  }

def get_super_user_id():
  return db.query(User).filter_by(email = 'superuser@gmail.com').first().id

def get_event_id():
    return db.query(Event).filter_by(owner_id = get_super_user_id()).first().id


def test_create_event():
  client = flask_app.test_client()
  request_params = {
    "title": "Event title1",
    "date": "2021/07/21 17:32:28"
  }
  response = client.post(
    '/events',
    json=request_params,
    headers=generate_header()
  )

  assert response.status_code == 201
  assert response.json
  assert response.json['title'] == request_params['title']

def test_get_events():
  client = flask_app.test_client()
  response = client.get(
    '/events',
    headers=generate_header()
  )

  assert response.status_code == 200
  assert response.json

def test_update_event():
  client = flask_app.test_client()
  request_params = {
    "title": "Event title updated",
    "date": "2021/07/21 17:32:28"
  }
  response = client.put(
    f"/events/{get_event_id()}",
    json=request_params,
    headers=generate_header()
  )

  assert response.status_code == 201
  assert response.json
  assert response.json['title'] == request_params['title']

def test_delete_events():
  client = flask_app.test_client()
  response = client.delete(
    f"/events/{get_event_id()}",
    headers=generate_header()
  )

  assert response.status_code == 202
  assert response.json

def test_add_user_to_event():
  client = flask_app.test_client()
  response = client.post(
    f"/events_by_user/{get_event_id()}/{get_super_user_id()}",
    headers=generate_header()
  )

  assert response.status_code == 201
  assert response.json


def test_get_events_by_user():
  client = flask_app.test_client()
  response = client.get(
    '/events_by_user',
    headers=generate_header()
  )

  assert response.status_code == 200
  assert response.json

def test_delete_user_from_event():
  client = flask_app.test_client()
  response = client.delete(
    f"/events_by_user/{get_event_id()}/{get_super_user_id()}",
    headers=generate_header()
  )

  assert response.status_code == 202
  assert response.json
