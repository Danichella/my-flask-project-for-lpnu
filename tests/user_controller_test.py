import json
from app import flask_app

def test_register_user():
  client = flask_app.test_client()
  request_params = {
    'email': "superuser@gmail.com",
    'password': "12345678",
    'username': "@alexx",
    'firstName': "Alex",
    'lastName': "Bodh",
  }
  response = client.post(
    '/register',
    json=request_params
  )

  assert response.status_code == 201
  assert response.json['message'] == 'Registered successfully'

def test_login_user():
  client = flask_app.test_client()
  request_params = {
    'email': "superuser@gmail.com",
    'password': "12345678",
  }
  response = client.post(
    '/login',
    json=request_params
  )

  assert response.status_code == 200
  assert response.json['token']
