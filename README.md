# My flask project for LPNU



## Instalation

### Install brew

```bash
xcode-select --install

curl -fsSL -o install.sh https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh
/bin/bash install.sh
```

### Install pyenv

```bash
brew install pyenv
```

### Installing python

```bash
pyenv install 3.6.0

# Install python localy
eval "$(pyenv init --path)"
pyenv local 3.6.0

# To check if python is installed successfully
python --version # Python 3.6.0
```

### Install virtualenv

```bash
# Install pip
python install pip

pip install virtualenv
virtualenv my-flask-project-for-lpnu
source my-flask-project-for-lpnu/bin/activate

# To dactivate run
deactivate

# To check if virtual env is active run
which python
```

### Install requirements
```bash
pip install -r requirements.txt
```

### Install flask and gunicorn

```bash
pip install flask gunicorn
```

### Run the server and see the output

```bash
gunicorn --bind localhost:5000 app:flask_app

curl -v -XGET http://localhost:5000/api/v1/hello-world-10
```

### Setup database

```bash
alembic upgrade head
```
