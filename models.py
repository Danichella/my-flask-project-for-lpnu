from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table, DateTime
from sqlalchemy.orm import sessionmaker, scoped_session, declarative_base
from sqlalchemy.sql import func

engine = create_engine(f"mysql+mysqlconnector://root:root@localhost:3306/calendar")
engine.connect()

SessionFactory = sessionmaker(bind=engine)
Session = scoped_session(SessionFactory)
Base = declarative_base()

class User(Base):
    __tablename__ = "User"

    id = Column(Integer(), primary_key=True)
    email = Column(String(100), nullable=False)
    password = Column(String(100), nullable=False)
    username = Column(String(100), nullable=False)
    firstName = Column(String(100), nullable=False)
    lastName = Column(String(100), nullable=False)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())


    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Event(Base):
    __tablename__ = "Event"

    id = Column(Integer(), primary_key=True)
    title = Column(String(100), nullable=False)
    date = Column(DateTime, nullable=False)
    owner_id = Column(Integer(), ForeignKey('User.id'))
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class User_Event(Base):
    __tablename__ = "User_Event"

    id = Column(Integer(), primary_key=True)
    user_id = Column(Integer(), ForeignKey('User.id'))
    event_id = Column(Integer(), ForeignKey('Event.id'))
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
