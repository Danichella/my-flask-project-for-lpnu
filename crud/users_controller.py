from models import *
from schemas import *
from app import db, bcrypt, app
from flask import jsonify
from flask_jwt import jwt
import datetime

def register_user(body: UserSchema):
  user = User(
    email = body['email'],
    password = bcrypt.generate_password_hash(body['password']),
    username = body['username'],
    firstName = body['firstName'],
    lastName = body['lastName']
  )

  db.add(user)
  db.commit()
  db.refresh(user)

  return jsonify({'message': 'Registered successfully'}), 201

def login_user(body: LoginSchema):
  if not body or not body['email'] or not body['password']: 
    return jsonify({'Error': 'Authentication failed'}), 401
 
  user = db.query(User).filter_by(email=body['email']).first()  
  if bcrypt.check_password_hash(user.password, body['password']):
      token = jwt.encode(
        {'id' : user.id,
        'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=45)},
        app.app.config['SECRET_KEY'],
        "HS256"
      )
      
      return jsonify({'token': str(token)}), 200

  return jsonify({'Error': 'Authentication failed'}), 401