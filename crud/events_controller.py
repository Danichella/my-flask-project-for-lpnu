# from sqlalchemy import and_
from models import *
from schemas import *
from app import db, token_required
from flask import jsonify

@token_required
def get_events(current_user):
  events = db.query(Event).filter_by(owner_id = current_user.id)

  return jsonify([e.as_dict() for e in events]), 200

@token_required
def add_event(current_user, body: EventSchema):
  event = Event(
    title = body['title'],
    date = body['date'],
    owner_id = current_user.id
  )

  db.add(event)
  db.commit()
  db.refresh(event)

  return jsonify(event.as_dict()), 201

@token_required
def update_event(current_user, event_id, body: EventSchema):
  event = db.query(Event).filter_by(owner_id = current_user.id, id = event_id).first()
  if not(bool(event)):
    return jsonify({'Error': 'Not found'}), 404

  event.title = body['title']
  event.date = body['date']

  db.merge(event)
  db.flush()
  db.commit()

  return jsonify(event.as_dict()), 201

@token_required
def delete_event(current_user, event_id):
  event = db.query(Event).filter_by(owner_id = current_user.id, id = event_id).first()
  if not(bool(event)):
    return jsonify({'Error': 'Not found'}), 404

  db.delete(event)
  db.commit()

  return jsonify(event.as_dict()), 202

@token_required
def get_events_by_user(current_user):
  user_events = db.query(User_Event).filter_by(user_id = current_user.id).all()

  events=[]
  for ue in user_events:
    events.append(db.query(Event).get(ue.event_id))

  return jsonify([e.as_dict() for e in events]), 200

@token_required
def add_user_to_event(current_user, event_id, user_id):
  user_event = User_Event(
    user_id = user_id,
    event_id = event_id
  )
  UserEventSchema().dump(user_event)
  if db.query(Event).get(event_id).owner_id != current_user.id:
    return jsonify({'Error': 'Permision denied'}), 403

  db.add(user_event)
  db.commit()
  db.refresh(user_event)

  return jsonify(user_event.as_dict()), 201

@token_required
def delete_user_from_event(current_user, event_id, user_id):
  user_event = db.query(User_Event).filter_by(user_id = user_id, event_id = event_id).first()
  if not(bool(user_event)):
    return jsonify({'Error': 'Not found'}), 404
  if db.query(Event).get(event_id).owner_id != current_user.id:
    return jsonify({'Error': 'Permision denied'}), 403

  db.delete(user_event)
  db.commit()

  return jsonify(user_event.as_dict()), 202