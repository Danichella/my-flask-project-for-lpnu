from marshmallow import Schema, fields

class UserSchema(Schema):
  email = fields.Email()
  password = fields.Str()
  username = fields.Str()
  firstName = fields.Str()
  lastName = fields.Str()

class EventSchema(Schema):
  title = fields.Str()
  date = fields.DateTime()

class UserEventSchema(Schema):
  user_id = fields.Int()
  event_id = fields.Int()

class LoginSchema(Schema):
  email = fields.Email()
  password = fields.Str()